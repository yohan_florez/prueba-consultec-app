import { Component } from '@angular/core';
import { Service } from '../services/service.module';
import { Plugins } from '@capacitor/core';

const { Geolocation } = Plugins;
declare var google;

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  map = null;
  markers:any = [];
  points:any = [];
  bounds: any = null;
  myPosition:any;
  position2: any = {
    lat : '',
    lng : ''
  };
  directionsService: any = null;
  directionsDisplay: any = null;

  constructor(private service: Service) {
    this.directionsService = new google.maps.DirectionsService();
    this.directionsDisplay = new google.maps.DirectionsRenderer();
    this.bounds = new google.maps.LatLngBounds();
  }

  ngOnInit(){
    this.getPoints();
  }

  getPoints(){
    this.service.getAll('location').subscribe(data =>{
      this.points = data;
      this.loadMap();
    })
  }

  async getCurrentPosition() {
    const coordinates = await Geolocation.getCurrentPosition();
    return {
      lat: coordinates.coords.latitude,
      lng: coordinates.coords.longitude
    };
  }

  async loadMap(){
    const mapEle: HTMLElement = document.getElementById('map');
    this.myPosition = await this.getCurrentPosition();
    this.map = new google.maps.Map(mapEle, {
      center: this.myPosition,
      zoom: 16
    });

    new google.maps.Marker({
      position: this.myPosition,
      map: this.map,
      icon: 'assets/icon/current.png',
    });
    this.directionsDisplay.setMap(this.map);
  
    google.maps.event.addListenerOnce(this.map, 'idle', () => {
      for(let p of this.points){
        let marker = new google.maps.Marker({
          position: {lat: parseFloat(p.latitude), lng: parseFloat(p.longitude)},
          map: this.map,
        });
        marker.addListener('click', e => {
          this.position2 = {lat: e.latLng.lat(), lng: e.latLng.lng()};
          this.map.panTo(e.latLng);
          for (let m of this.markers)
            this.calculateRoute();
        });
        this.markers.push(marker)
      }
    });
  }

  private calculateRoute(){
    var point = new google.maps.LatLng(this.myPosition.lat, this.myPosition.lng);
    this.bounds.extend(point);
    var point2 = new google.maps.LatLng(this.position2.lat, this.position2.lng);
    this.bounds.extend(point2);

    this.map.fitBounds(this.bounds);
    this.directionsService.route({
      origin: new google.maps.LatLng(this.myPosition.lat, this.myPosition.lng),
      destination: new google.maps.LatLng(this.position2.lat, this.position2.lng),
      optimizeWaypoints: true,
      travelMode: google.maps.TravelMode.DRIVING,
      avoidTolls: true
    }, (response, status)=> {
      if(status === google.maps.DirectionsStatus.OK) {
        this.directionsDisplay.setDirections(response);
      }
    });  
  }

}
