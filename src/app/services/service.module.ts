import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable()

export class Service {

  baseUrl:string = "https://ct-api.ebes.com.ar/api/v1";

  constructor(private http:HttpClient) { }

  getAll(entity) {
    const url = this.baseUrl + '/' + entity;
    return this.http.get(url, { headers: this.getHeaders() });
  }

  private getHeaders() {
    let headers = new HttpHeaders({
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }); 
    return headers;
  }

} 